﻿using System;
using static System.Console;

namespace _542
{
    class Program
    {
        static void Main()
        {
            int chislo, i, summaBalov;
            i = 0;
            summaBalov = 0;

            do
            {
                Write("Введите " + (i + 1) + " оценку: ");
                chislo = Convert.ToInt32(ReadLine());

                summaBalov = summaBalov + chislo;
                i = i + 1;

            } while (i < 4);

            WriteLine("\nСумма балов введенных оценок = " + summaBalov);
            ReadKey();
        }
    }
}
