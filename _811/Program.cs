﻿using System;
using static System.Console;

namespace _811
{
    class Program
    {
        static void Main(string[] args)
        {
            int rabotnik, month, sumKvartVse, zarplata, sumKvartPoRabot, sumMonth1, sumMonth2, sumMonth3;
            rabotnik = month = 1;
            sumKvartPoRabot = sumKvartVse = 0;
            sumMonth1 = sumMonth2 = sumMonth3 = 0;

            while (rabotnik <= 12)
            {

                while (month <= 3)
                {
                    Write("Введите зарплату за " + month + "-й месяц: ");
                    zarplata = Convert.ToInt32(ReadLine());
                    sumKvartVse += zarplata;
                    sumKvartPoRabot += zarplata;

                    if (month == 1) sumMonth1 += zarplata;
                    else if (month == 2) sumMonth2 += zarplata;
                    else sumMonth3 += zarplata;

                    month++;
                }

                WriteLine("Сумма за квартал работника (" + rabotnik + ") = " + sumKvartPoRabot);
                month = 1;
                sumKvartPoRabot = 0;
                rabotnik++;
            }

            WriteLine($"\nЗарплата всех рабочих за месяца: 1-> {sumMonth1} 2-> {sumMonth2} 3-> {sumMonth3}");
            WriteLine($"\nСумма всех денег работников = {sumKvartVse}");
            ReadKey();

        }
    }
}
