﻿using System;
using static System.Console;

namespace _131
{
    class Program
    {
        static void Main()
        {
            int N, V, S;
            int i = 1;          //счетчик для цикла N
            int nV = 0;         //общее количество людей
            int nVmuz = 0;      //общее кол. мужчин
            int maxVmuz = 0;    // максим. возраст мужчины
            int nVmuzMax = 0;   // поряд. номер мужчины с большим возрастом

            Write("Введите число жильцов N = ");
            N = Convert.ToInt32(ReadLine());

            while (i <= N)
            {
                Write("\nВведите возраст жильца = ");
                V = Convert.ToInt32(ReadLine());
                do
                {
                    Write("Введите пол жильца М=1 или Ж=0 = ");
                    S = Convert.ToInt32(ReadLine());

                } while (S != 1 && S != 0);
                nV = nV + 1;

                if (S == 1)
                {
                    nVmuz = nVmuz + 1;

                    if (V > maxVmuz)
                    {
                        maxVmuz = V;
                        nVmuzMax = nV;
                    }

                }
                
                i = i + 1;
            }

            if (nVmuz > 0)
            {
                WriteLine(nVmuzMax);
            }
            else
            {
                WriteLine("-1");
            }
                        
            ReadKey();
        }
    }
}
