﻿using System;
using static System.Console;

namespace _9
{
    class Program
    {
        static void Main()
        {
            int a, b;
            a = 1;
            b = 1;

            while (b <= 5)
            {
                while (a <= b)
                {
                    Write(b * 10 + " ");
                    a = a + 1;
                }

                WriteLine();
                a = 1;
                b = b + 1;
            }
            

            ReadKey();
        }
    }
}
