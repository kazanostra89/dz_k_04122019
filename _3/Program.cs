﻿using System;
using static System.Console;

namespace _3
{
    class Program
    {
        static void Main()
        {
            int i = 0; //счетчик
            int f0, f1, summaFibon;
            f0 = 0;
            f1 = 1;
            WriteLine(f0 + "\n" + f1);

            while (i < 8)
            {
                summaFibon = f0 + f1;
                f0 = f1;
                f1 = summaFibon;
                WriteLine(summaFibon);
                i = i + 1;
            }

            ReadKey();
        }
    }
}
